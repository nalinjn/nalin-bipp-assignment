# nalin-bipp-assignment

This application is based on mithri.js and tachyons css framework. It showcases a random generated data points in a table structure. The view can be adjusted with the following functions:

	a. the no. of rows
	b. the starting row from the table


## What is Mithril.js
Mithril is a modern client-side Javascript framework for building Single Page Applications. It's small, fast and provides routing and XHR utilities out of the box. It's an open source project.
